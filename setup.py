import sys
import numpy as np
import math
import random

import gym


def simulate():

    # Instanciamos los parametros relacionados con el aprendizaje
    learning_rate = get_learning_rate(0) # gamma
    explore_rate = get_explore_rate(0) # epsilon

    # Instanciamos la racha de veces conseguido
    num_streaks = 0

    # Renderizamos el tablero
    env.render()

    # Iteramos el numero de episodios deseado
    for episode in range(NUM_EPISODES):

        # Reseteamos el entorno
        obv = env.reset()

        # Declaramos el estado inicial
        state_0 = state_to_bucket(obv)
        total_reward = 0

        # En cada uno de los pasos
        for t in range(MAX_T):

            # Seleccionamos una accion
            action = select_action(state_0, explore_rate)

            # Ejecutamos la accion
            obv, reward, done, _ = env.step(action)

            # Seteamos el nuevo estado devuelto en la linea anterior y actualizamos la recompensa
            state = state_to_bucket(obv)
            total_reward += reward

            # Actualizamos el valor Q para dicho estado y accion basado en resultado de tomar el paso
            best_q = np.amax(q_table[state])
            q_table[state_0 + (action,)] += learning_rate * (reward +
                                                             (best_q) - q_table[state_0 + (action,)])

            # Seteamos el estado para la siguiente iteracion
            state_0 = state

            # Informacion de debug
            if DEBUG_MODE == 2:
                print("\nEpisode = %d" % episode)
                print("t = %d" % t)
                print("Action: %d" % action)
                print("State: %s" % str(state))
                print("Reward: %f" % reward)
                print("Best Q: %f" % best_q)
                print("Explore rate: %f" % explore_rate)
                print("Learning rate: %f" % learning_rate)
                print("Streaks: %d" % num_streaks)
                print("")

            elif DEBUG_MODE == 1:
                if done or t >= MAX_T - 1:
                    print("\nEpisode = %d" % episode)
                    print("t = %d" % t)
                    print("Explore rate: %f" % explore_rate)
                    print("Learning rate: %f" % learning_rate)
                    print("Streaks: %d" % num_streaks)
                    print("Total reward: %f" % total_reward)
                    print("")

            # Volvemos a renderizar el tablero
            if RENDER_MAZE:
                env.render()

            # Comprobamos si hemos finalizado
            if env.is_game_over():
                sys.exit()

            # Si hemos alcanzado el objetivo
            if done:
                # Imprimimos informacion acerca del numero de pasos, la recompensa y la racha actual
                print("Episode %d finished after %f time steps with total reward = %f (streak %d)."
                      % (episode, t, total_reward, num_streaks))

                # Consideramos que este episodio suma para la racha si se consigue en menos pasos de SOLVED_T
                if t <= SOLVED_T:
                    num_streaks += 1
                else:
                    num_streaks = 0
                break
            
            # En caso de que el objetivo no se alcance en MAX_T pasos, consideraremos un timeout
            elif t >= MAX_T - 1:
                print("Episode %d timed out at %d with total reward = %f."
                      % (episode, t, total_reward))

        # Consideraremos resuelto si la racha es superior a STREAK_TO_END (100)
        if num_streaks > STREAK_TO_END:
            break

        # Actualizamos los parametros de aprendizaje (gamma y epsilon)
        explore_rate = get_explore_rate(episode)
        learning_rate = get_learning_rate(episode)


# Seleccionar accion
def select_action(state, explore_rate):
    # Selecciona accion aleatoria (exploracion) segun epsilon
    if random.random() < explore_rate:
        action = env.action_space.sample()
    # Seleccionamos la accion con el valor Q mas alto
    else:
        action = int(np.argmax(q_table[state]))
    return action

'''
Para actualizar nuestro epsilon y gamma haremos uso de la siguiente expresion
    |  MIN_LEARNING/EXPLORE_RATE
max |       
    |       | INITIAL_LEARNING/EXPLORE_RATE
    |  min  |
            | 1 - log10((t+1) / DECAY_FACTOR)
'''
def get_explore_rate(t):
    return max(MIN_EXPLORE_RATE, min(INITIAL_EXPLORE_RATE, 1.0 - math.log10((t+1)/DECAY_FACTOR)))


def get_learning_rate(t):
    return max(MIN_LEARNING_RATE, min(INITIAL_LEARNING_RATE, 1.0 - math.log10((t+1)/DECAY_FACTOR)))

# Utilidad para mapear un estado a su posicion en nuestra tabla de valores Q
def state_to_bucket(state):
    bucket_indice = []
    for i in range(len(state)):
        if state[i] <= STATE_BOUNDS[i][0]:
            bucket_index = 0
        elif state[i] >= STATE_BOUNDS[i][1]:
            bucket_index = NUM_BUCKETS[i] - 1
        else:
            # Mapping the state bounds to the bucket array
            bound_width = STATE_BOUNDS[i][1] - STATE_BOUNDS[i][0]
            offset = (NUM_BUCKETS[i]-1)*STATE_BOUNDS[i][0]/bound_width
            scaling = (NUM_BUCKETS[i]-1)/bound_width
            bucket_index = int(round(scaling*state[i] - offset))
        bucket_indice.append(bucket_index)
    return tuple(bucket_indice)


if __name__ == "__main__":

    # Inicializamos el entorno (tablero)
    env = gym.make("gym_maze:maze-rl-10x6-v0")

    '''
    Variables relacionadas con el entorno
    '''
    # Tamaño del tablero en tubla (10,6)
    MAZE_SIZE = tuple((env.observation_space.high +
                       np.ones(env.observation_space.shape)).astype(int))

    # Un bucket por posicion del tablero
    NUM_BUCKETS = MAZE_SIZE

    # Numero de acciones
    NUM_ACTIONS = env.action_space.n  # ["N", "S", "E", "W"]

    # Cada uno de los posibles estados (utilidad state_to_bucket)
    STATE_BOUNDS = list(zip(env.observation_space.low,
                            env.observation_space.high))

    '''
    Constantes de aprendizaje
    '''
    # Ratio minimo de exploracion (epsilon minimo)
    MIN_EXPLORE_RATE = 0.001
    
    # Ratio minimo de aprendizaje (gamma minimo)
    MIN_LEARNING_RATE = 0.5

    # Ratio inicial de exploracion (epsilon)
    INITIAL_EXPLORE_RATE = 0.65

    # Ratio inicial de aprendizaje (gamma)
    INITIAL_LEARNING_RATE = 0.8

    # Factor de disminucion de ratios previos
    DECAY_FACTOR = np.prod(MAZE_SIZE, dtype=float) / 10.0

    '''
    Constantes relacionadas con la simulacion
    '''
    # Numero maximo de episodios (epocas) en caso de no alcanzar las rachas objetivo
    NUM_EPISODES = 50000
    
    # Numero maximo de pasos antes de considerar timeout, depende del tamaño del tablero
    MAX_T = np.prod(MAZE_SIZE, dtype=int) * 100

    # Racha a alcanzar para considerar resuelto
    STREAK_TO_END = 100

    # Pasos T a dar para considerar que suma a la racha (resuelto)
    SOLVED_T = np.prod(MAZE_SIZE, dtype=int)

    # Modo de debug, usa 1 o 2 para ver informacion extra
    DEBUG_MODE = 0

    # Parametro de control para lanzar el renderizado del tablero (pygame)
    RENDER_MAZE = True

    '''
    Creamos una tabla de 0s para cada par estado-accion
    '''
    q_table = np.zeros(NUM_BUCKETS + (NUM_ACTIONS,), dtype=float)

    '''
    Lanzamos la simulacion
    '''
    simulate()
